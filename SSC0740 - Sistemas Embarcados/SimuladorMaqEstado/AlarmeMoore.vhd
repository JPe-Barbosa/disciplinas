LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY AlarmeMoore IS
	PORT
	(
		Botao		: IN STD_LOGIC ;
		Porta			: IN STD_LOGIC ;
		Clock		: IN STD_LOGIC ;
		Reset		: IN STD_LOGIC ;
		LED			: OUT STD_LOGIC; 
		Bz			: OUT STD_LOGIC;
		Display	: OUT STD_LOGIC_VECTOR(7 DOWNTO 0)	
	);
END AlarmeMoore;


ARCHITECTURE Barney OF AlarmeMoore IS

BEGIN

PROCESS (Clock, Reset)

variable state : STD_LOGIC_VECTOR(7 DOWNTO 0);

BEGIN
	if (Reset='1') then
		Estado:=x"00";
	
	elsif (Clock'event and Clock='1') then
		case Estado is  -- Logica de Transicao
			when x"00" =>  -- Desativado
				if (Botao='1') then Estado:=x"01"; end if;
			when x"01" =>  --Ativado
				if (Botao='1') then Estado:=x"00"; end if;
				if (Porta='1') then Estado:=x"02"; end if;
			when x"02" =>  -- Disparou
				if (Botao='1') then Estado:=x"00"; end if;
			when OTHERS =>
				Estado:=x"00";
		end case;
		
		case Estado is  -- Logica de Acao
			when x"00" =>  
				LED <= '0';
				Bz <= '0';
			when x"01" =>
				LED <= '1';
				Bz <= '0';
			when x"02" =>
				LED <= '1';
				Bz <= '1';
			when OTHERS =>
				LED <= '0';
				Bz <= '0';
		end case;		
		
		
	end if;

	Display <= Estado;
	
END PROCESS;

	
END Barney;
